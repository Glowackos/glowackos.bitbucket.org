# Bored Aussie Members #

Provides an interactive way of exploring the Bored Aussie member tree. Links represent invites.

### Running server-backed version ###

* Start up server with node: 'node server.js'. It should take around a minute to start up.
* Connect to server through the browser by going to the URL: <serverIPAddressOrName>:6119
* The webpage will be returned and should load immediately.

### Running standalone version ###

* Go to the URL: glowackos.bitbucket.org/BA_FamilyTree