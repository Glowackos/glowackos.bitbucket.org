var argv = require('minimist')(process.argv.slice(2))
var ip = argv._[0] || "0.0.0.0"
var port = argv.port || argv.p || argv._[1] || 6119

var fs = require('fs')

var app = require('express')()
var server = require('http').Server(app)
var io = require('socket.io')(server);

server.listen(port, ip, function() {
	console.log("Local server running on: " + ip + ":" + port)
})

app.get('/', function (req, res) {
	console.log("Index requested")
	res.sendFile(__dirname + '/fast.html')
})
app.get('/style.css', function (req, res) {
	console.log("Stylesheet requested")
	res.sendFile(__dirname + '/style.css')
})
app.get('/socket.io/socket.io.js', function (req, res) {
	console.log("Stylesheet requested")
	res.sendFile(__dirname + '/style.css')
})

var MEMBERS = {}
MEMBERS["<nobody>"] = {
			ID: -1,
			Username: "<nobody>",
			Date_joined: "N/A",
			Invited_by: "N/A",
			Invited: []
		}
readXML(1, 44)

io.on('connection', function(socket) {
	console.log("Client connected. socketId: " + socket.id)
	socket.emit('message', 'Welcome');
	
	socket.on('member', function(username){
		console.log("Client " + socket.id + " requested member data for: " + username)
		if(MEMBERS[username]){
			console.log("Member data sent!")
			socket.emit('member', MEMBERS[username])
		} else {
			console.log("No member with this name!")
			socket.emit('member', null)
		}
	})
	
	
	socket.on('error', console.error)
	
	socket.on('disconnect', function(reason){
		console.log("Client disconnected. socketId: " + socket.id + ", reason: " + reason)
	})
})

function readXML(fileID, max){
	var xml = loadXMLDoc("userdata/" + fileID + ".xml")
	addUsersFromXML(xml)
	console.log('Successfully processed file: ' + fileID + '.xml')
	if(fileID < max){
		readXML(fileID+1, max)
	} else {
		console.log("Finished loading!")
	}
}

function addUsersFromXML(xml){
	users = xml.Memberlist_Information_Retriever.User
	for(var i = 0; i < users.length; i++) {
		var user = users[i]
		if(user.Username[0] == "N/A") continue
		usernamelower = user.Username[0].toLowerCase()
		var invitedBy = (user.Invited_by[0] == "nobody")? "<nobody>" : user.Invited_by[0]
		if(MEMBERS[usernamelower]){
			MEMBERS[usernamelower].ID = user.ID[0]
			MEMBERS[usernamelower].Username = user.Username[0]
			MEMBERS[usernamelower].Date_joined = user.Date_joined[0]
			MEMBERS[usernamelower].Invited_by = invitedBy
		} else {
			MEMBERS[usernamelower] = {
				ID: user.ID[0],
				Username: user.Username[0],
				Date_joined: user.Date_joined[0],
				Invited_by: invitedBy,
				Invited: []
			}
		}
		invitedbylower = invitedBy.toLowerCase()
		if(MEMBERS[invitedbylower]){
			MEMBERS[invitedbylower].Invited.push(usernamelower)
		} else {
			MEMBERS[invitedbylower] = {
				Invited: [usernamelower]
			}
		}
	}
}

function loadXMLDoc(filePath) {
	var xml2js = require('xml2js')
	var json
	try {
		var fileData = fs.readFileSync(filePath, 'ascii')

		var parser = new xml2js.Parser()
		parser.parseString(fileData.substring(0, fileData.length), function (err, result) {
			json = result
		})
		return json
	} catch (ex) {console.log(ex)}
}