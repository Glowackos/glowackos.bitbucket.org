var fs = require('fs');

var MEMBERS = {};
MEMBERS["nobody"] = {
			ID: -1,
			Username: "nobody",
			Date_joined: "N/A",
			Invited_by: "N/A",
			Invited: []
		};
var VIEWGRAPH = {nodes: [], links: []};

function readXML(fileID, max){
	var xml = loadXMLDoc("userdata/" + fileID + ".xml");
	addUsersFromXML(xml);
	console.log('Successfully processed file: ' + fileID + '.xml');
	if(fileID < max){
		readXML(fileID+1, max);
	} else {
		begin();
	}
}

readXML(1, 44);

function begin(){
	console.log("Finished loading!");
	fs.writeFile("userdata/processed.json", JSON.stringify(MEMBERS), function(err){
		if(err) {
			console.log(err);
		} else {
			console.log("File saved!");
		}
	});
}

function addUsersFromXML(xml){
	users = xml.Memberlist_Information_Retriever.User;
	for(var i = 0; i < users.length; i++) {
		var user = users[i];
		if(user.Username[0] == "N/A") continue;
		usernamelower = user.Username[0].toLowerCase();
		var invitedBy = (user.Invited_by[0] == "nobody")? "<nobody>" : user.Invited_by[0]
		if(MEMBERS[usernamelower]){
			MEMBERS[usernamelower].ID = user.ID[0];
			MEMBERS[usernamelower].Username = user.Username[0];
			MEMBERS[usernamelower].Date_joined = user.Date_joined[0];
			MEMBERS[usernamelower].Invited_by = invitedBy
		} else {
			MEMBERS[usernamelower] = {
				ID: user.ID[0],
				Username: user.Username[0],
				Date_joined: user.Date_joined[0],
				Invited_by: invitedBy,
				Invited: []
			};
		}
		invitedbylower = invitedBy.toLowerCase();
		if(MEMBERS[invitedbylower]){
			MEMBERS[invitedbylower].Invited.push(usernamelower);
		} else {
			MEMBERS[invitedbylower] = {
				Invited: [usernamelower]
			};
		}
	}
}

function loadXMLDoc(filePath) {
	var xml2js = require('xml2js');
	var json;
	try {
		var fileData = fs.readFileSync(filePath, 'ascii');

		var parser = new xml2js.Parser();
			parser.parseString(fileData.substring(0, fileData.length), function (err, result) {
			json = result;
		});

		return json;
	} catch (ex) {console.log(ex)}
}